# tfkit - Shellfu's movable test framework
# See LICENSE file for copyright and license details.
#
# To use, set TF_DIR (make macro) in your Makefile and
# include this file.
#
# Adds `test` target.
#
# Note that running "runtests" directly should have the same
# effect but you will need to set TF_DIR and/or TF_SUITE properly
# unless they are default (see README)

export TF_DIR

test:
	@$(TF_DIR)/runtests

.PHONY: test
