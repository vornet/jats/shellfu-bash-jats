TODO
====

 *  Add way to pass:
     *  test name, etc.
     *  env info,
     *  sut info.

 *  think hard about promises

 *  do YAML more right

 *  wait, are phases preserved across runs?

 *  WAIT, are phases preserved into subshell (logging..)?

 *  jat__lsummary()

 *  add .j2 for HTML/text/whatever (separate module?)

 *  fortify against running from within test
